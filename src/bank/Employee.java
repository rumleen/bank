/*
 * Rumleen Rathor
 * Student ID: 991334196
 * SYST10199 - Web Programming
 */
package bank;

/**
 *
 * @author Rumleen Rathor Product Version: Apache NetBeans IDE 12.3
 * <your.name at your.org>
 */
public class Employee {
     private String name;
        private Bank bank;
        /**
         * @param name
         * @param bank
         */
        public Employee(String name, Bank bank) {
                super();
                this.name = name;
                this.bank = bank;
        }
}
